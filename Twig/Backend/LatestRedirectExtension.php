<?php
namespace Innomedio\RedirectBundle\Twig\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\EmailBundle\Entity\Mail;
use Innomedio\RedirectBundle\Entity\Redirect;
use Innomedio\RedirectBundle\Entity\RedirectHit;
use Twig\TwigFunction;

class LatestRedirectExtension extends \Twig_Extension
{
    private $em;

    /**
     * LatestEmailsExtension constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('latest404pages', array($this, 'latest404pages'))
        );
    }

    /**
     * @param $amount
     * @return array|RedirectHit[]
     */
    public function latest404pages($amount = 5)
    {
        return $this->em->getRepository('InnomedioRedirectBundle:RedirectHit')->findBy(array('result' => null), array('hitDate' => 'desc'), $amount);
    }
}