<?php
namespace Innomedio\RedirectBundle\Service\Backend;

use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarExtension;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarItem;
use Symfony\Component\Routing\Router;

class RedirectBackendSidebar extends SidebarExtension
{
    private $sidebarContainer;
    private $router;

    /**
     * SidebarItems constructor.
     * @param SidebarContainer $sidebarContainer
     * @param Router $router
     */
    public function __construct(SidebarContainer $sidebarContainer, Router $router)
    {
        $this->sidebarContainer = $sidebarContainer;
        $this->router = $router;
    }

    /**
     * @return array|SidebarItem
     */
    public function getSidebars()
    {
        return array(
            $this->translations()
        );
    }

    /**
     * @return SidebarItem
     */
    public function translations()
    {
        $admin = $this->sidebarContainer->getMainItem('admin');

        $email = new SidebarItem();
        $email->setIcon('fa-exchange');
        $email->setName('innomedio.redirect.redirects');
        $email->setLink($this->router->generate('innomedio.redirect.index'));
        $email->setTag('redirect');
        $email->setParent($admin);
        $email->setRole('ROLE_REDIRECT');

        return $email;
    }
}