<?php
namespace Innomedio\RedirectBundle\Service\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Service\Message\Message;
use Innomedio\BackendThemeBundle\Service\Message\MessageExtension;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

class RedirectDashboardMessage extends MessageExtension
{
    private $translator;
    private $em;
    private $router;

    /**
     * MissingTranslationsMessage constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, RouterInterface $router)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->router = $router;
    }

    /**
     * @return array|Message[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMessages()
    {
        return array(
            $this->hasErrorsMessage()
        );
    }

    /**
     * @return Message|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function hasErrorsMessage()
    {
        $check = $this->em->getRepository('InnomedioRedirectBundle:Redirect')->countUrlsWithoutRedirect();

        if ($check) {
            $message = new Message();
            $message->setText($this->translator->trans('innomedio.redirect.dashboard.error_pages'));
            $message->setLink($this->router->generate('innomedio.redirect.index'));
            $message->setRole('ROLE_REDIRECT');
            $message->setType('warning');
            $message->setIcon('fa fa-exchange');

            return $message;
        }

        return null;
    }
}