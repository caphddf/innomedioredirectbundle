# InnomedioRedirectBundle

* Stores all 404 hits
* You can set up a redirect link for all those hits in the backend

## Documentation

* [Installation](Resources/docs/installation.md)
* [Dashboard](Resources/docs/dashboard.md)