# Installation

**Make sure the InnomedioBackendThemeBundle is installed correctly.**

Add the bundle to your composer file: 

```
"require": {
    "innomedio/redirect-bundle": "^1.0"
},

"repositories": [
    {
        "type": "git",
        "url": "git@gitlab.com:innomedio/internal/symfony/InnomedioRedirectBundle.git"
    }
]
```

Add the routing file to your **config/routes/annotations.yaml** file:

```
innomedio_redirect:
    resource: '@InnomedioRedirectBundle/Resources/config/routes.yaml'
```

Generate all tables:

```
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate
```

Make sure this role is available: **ROLE_REDIRECT** to be able to get to the backend part.

Finally, make sure that the Timestampable Doctrine extension is activated:

```
stof_doctrine_extensions:
    orm:
        default:
            timestampable: true
```