# Dashboard

A dashboard element is available for showing the latest 404 hits:

```
{% if is_granted('ROLE_REDIRECT') %}
    {% include "@InnomedioRedirect/backend/dashboard/latest_errors.html.twig" with {
        'amount': 5
    } %}
{% endif %}
```