<?php
namespace Innomedio\RedirectBundle\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\RedirectBundle\Entity\Redirect;
use Innomedio\RedirectBundle\Entity\RedirectHit;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private $em;
    private $requestStack;

    private $ignore = array(
        '/_wdt/',
        '/_profiler/'
    );

    /**
     * ExceptionSubscriber constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => array(
                array('notifyException', -10),
            )
        );
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function notifyException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof NotFoundHttpException) {
            $currentRequest = $this->requestStack->getCurrentRequest();

            $url = explode("?", $currentRequest->getRequestUri())[0];

            if (!in_array($url, $this->ignore)) {
                $error = $this->em->getRepository('InnomedioRedirectBundle:Redirect')->findOneBy(array('errorUrl' => $url));

                if (!$error) {
                    $error = new Redirect();
                    $error->setErrorUrl($url);

                    $this->em->persist($error);
                    $this->em->flush();
                }

                $newHit = new RedirectHit();
                $newHit->setRedirect($error);
                $newHit->setIp($currentRequest->getClientIp());
                $newHit->setResult($error->getRedirectUrl());

                $this->em->persist($newHit);
                $this->em->flush();

                if ($error->getRedirectUrl()) {
                    $event->setResponse(new RedirectResponse($error->getRedirectUrl(), 301));
                }
            }
        }
    }
}