<?php
namespace Innomedio\RedirectBundle\Controller\Backend;
use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\RedirectBundle\Entity\Redirect;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_REDIRECT')")
 * @Route("/admin/redirects")
 */
class RedirectController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.redirect.index")
     * @return Response
     */
    public function list()
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.redirect.redirects');

        return $this->render('@InnomedioRedirect/backend/list.html.twig', array(
            'redirects' => $this->getDoctrine()->getRepository('InnomedioRedirectBundle:Redirect')->findAll()
        ));
    }

    /**
     * @Route("/view/{id}", requirements={"id" = "\d+"}, name="innomedio.redirect.view")
     * @param Redirect $redirect
     * @return Response
     */
    public function view(Redirect $redirect)
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.redirect.redirects', $this->generateUrl('innomedio.redirect.index'));
        $this->header()->addBreadcrumb('innomedio.redirect.view_title');

        return $this->render('@InnomedioRedirect/backend/view.html.twig', array(
            'redirect' => $redirect,
            'hits' => $this->getDoctrine()->getRepository('InnomedioRedirectBundle:RedirectHit')->findBy(array('redirect' => $redirect), array('hitDate' => 'desc'))
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.redirect.submit"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function submit($id, Request $request)
    {
        $response = new AjaxResponse();

        $redirectUrl = $request->request->get('redirect');

        if (substr($redirectUrl, 0, 1) !== '/') {
            $redirectUrl = '/' . $redirectUrl;
        }

        if ($redirectUrl === '/') {
            $redirectUrl = null;
        }

        $redirect = $this->getDoctrine()->getRepository('InnomedioRedirectBundle:Redirect')->find($id);
        $redirect->setRedirectUrl($redirectUrl);

        $em = $this->getDoctrine()->getManager();
        $em->persist($redirect);
        $em->flush();

        $response->setSuccess(true);
        $response->setRedirect($this->generateUrl('innomedio.redirect.index'));
        $response->setMessage($this->get('translator')->trans('innomedio.redirect.edit_success'));

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.redirect.remove")
     * @param Redirect $redirect
     * @return JsonResponse
     */
    public function ajaxRemove(Redirect $redirect)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($redirect);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }
}