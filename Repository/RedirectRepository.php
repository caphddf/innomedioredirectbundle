<?php

namespace Innomedio\RedirectBundle\Repository;

use Doctrine\ORM\EntityRepository;

class RedirectRepository extends EntityRepository
{
    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countUrlsWithoutRedirect()
    {
        return $this
            ->createQueryBuilder('r')
            ->where("r.redirectUrl IS NULL")
            ->orWhere('r.redirectUrl = :empty')
            ->setParameter('empty', '')
            ->setMaxResults(1)
            ->getQuery()
            ->getScalarResult()
        ;
    }
}
