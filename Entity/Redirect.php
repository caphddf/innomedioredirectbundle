<?php
namespace Innomedio\RedirectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Redirect
 * @ORM\Table(name="redirect")
 * @ORM\Entity(repositoryClass="Innomedio\RedirectBundle\Repository\RedirectRepository")
 */
class Redirect
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="error_url", type="string")
     */
    private $errorUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="redirect_url", type="string", nullable=true)
     */
    private $redirectUrl;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Innomedio\RedirectBundle\Entity\RedirectHit", mappedBy="redirect", cascade={"persist", "remove"})
     */
    private $hits;

    /**
     * Redirect constructor.
     */
    public function __construct()
    {
        $this->hits = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getErrorUrl(): string
    {
        return $this->errorUrl;
    }

    /**
     * @param string $errorUrl
     */
    public function setErrorUrl(string $errorUrl): void
    {
        $this->errorUrl = $errorUrl;
    }

    /**
     * @return string|boolean
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param string|null $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return Collection
     */
    public function getHits(): Collection
    {
        return $this->hits;
    }

    /**
     * @param Collection $hits
     */
    public function setHits(Collection $hits): void
    {
        $this->hits = $hits;
    }
}