<?php
namespace Innomedio\RedirectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Redirect
 * @ORM\Table(name="redirect_hit")
 * @ORM\Entity(repositoryClass="Innomedio\RedirectBundle\Repository\RedirectHitRepository")
 */
class RedirectHit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Redirect
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\RedirectBundle\Entity\Redirect", inversedBy="hits")
     * @ORM\JoinColumn(name="redirect_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $redirect;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="hit_date")
     */
    private $hitDate;

    /**
     * @var string|boolean
     *
     * @ORM\Column(name="result", type="string", nullable=true)
     */
    private $result;

    /**
     * @var string|boolean
     *
     * @ORM\Column(name="ip", length=45, type="string", nullable=true)
     */
    private $ip;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Redirect
     */
    public function getRedirect(): Redirect
    {
        return $this->redirect;
    }

    /**
     * @param Redirect $redirect
     */
    public function setRedirect(Redirect $redirect): void
    {
        $this->redirect = $redirect;
    }

    /**
     * @return \DateTime
     */
    public function getHitDate(): \DateTime
    {
        return $this->hitDate;
    }

    /**
     * @param \DateTime $hitDate
     */
    public function setHitDate(\DateTime $hitDate): void
    {
        $this->hitDate = $hitDate;
    }

    /**
     * @return bool|string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param bool|string $result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }

    /**
     * @return bool|string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param bool|string $ip
     */
    public function setIp($ip): void
    {
        $this->ip = $ip;
    }
}