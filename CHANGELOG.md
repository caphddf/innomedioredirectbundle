# 1.0.2

* Some docs changes
* Changed all template includes to lowercase dirs

# 1.0.1

* Changed route file for BackendThemeBundle 1.1 change (subdomain) and the documentation for implementation.
* Removed the unnecessary setup command

# 1.0

* Start